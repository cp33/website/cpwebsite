var modalWrap = null;
var secretMessages;
/**
 * @param {string} title 
 * @param {string} description content of modal body 
 * @param {string} yesBtnLabel label of Yes button 
 * @param {string} noBtnLabel label of No button 
 * @param {function} callback callback function when click Yes button
 */
const showModal = (title = "Введите описание маркера", description, yesBtnLabel = 'Сохранить', noBtnLabel = 'Закрыть', callback) => 
{
  if (modalWrap !== null) 
  {
    modalWrap.remove();
  }

  modalWrap = document.createElement('div');
  modalWrap.innerHTML = `
    <div class="modal fade hide" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header bg-light">
            <h5 class="modal-title">${title}</h5>
          </div>
          <div class="modal-body">
            <textarea id="markerdescription" class="form-control" id="message-text"></textarea>
          </div>
          <div class="modal-footer bg-light">
            <button type="button" id="savebutton" class="btn btn-primary modal-success-btn" data-bs-dismiss="modal">${yesBtnLabel}</button>
          </div>
        </div>
      </div>
    </div>
  `;

  modalWrap.querySelector('.modal-success-btn').onclick = callback;

  document.body.append(modalWrap);

  var modal = new bootstrap.Modal(modalWrap.querySelector('.modal'), { backdrop: 'static', keyboard: false });
  modal.show();
}

let map;
let markers = [];
let pptt;

const novosibirsk = { lat: 55.008354, lng: 82.935730 };

function initMap() {
  map = new google.maps.Map(document.getElementById("map"), 
  {
    zoom: 10,               // Начальный масштаб
    center: novosibirsk,    // Центр по умолчанию Новосибирск
    disableDefaultUI: true, // Отключения стандартных инструментов карты ползунка приближение, спутник и т.д.
  });

  // Если происходит клик в "свободные координаты на карте" запускается функция
  // добавления маркера в это место 
  map.addListener("click", (event) => { 
    addMarker(event.latLng);
  });

}

// Добавление маркера на карту
function addMarker(location) {
  const marker = new google.maps.Marker({
    position: location,
    map: map,
  });
  showModal();

  // При нажатии кнопки "Сохранить" модального окна введенная информация о маркере записывается в переменную SecretMessage
  // Затем происходит вызов функции для привяки к данному маркеру информационного окна и методов взаимодействия
  savebutton.onclick = function() 
  {
    SecretMessage = document.getElementById('markerdescription').value;
    attachSecretMessage(marker, SecretMessage);
  }; 
}

// Добавление текущему маркеру информационного окна и взаимодействие с ним
function attachSecretMessage(marker, secretMessage) {

  // В информационное окно текущего маркера записываем необходимое содержание (пока что - это просто строка secretMessage)
  const infowindow = new google.maps.InfoWindow({
    content: secretMessage,
  });
  // При нажатии на маркер инициируется отображение информационного окна 
    marker.addListener("click", () => {
    infowindow.open(marker.get("map"), marker);
  });
  // При двойном нажатии на маркер инициируется удаления маркера с карты 
    marker.addListener("dblclick", () => {
    marker.setMap(null);
  });
}
